/* eslint-disable no-extend-native */
function deletrea() {
  let palabra = this;
  return palabra.split('').map(letra => letra.toLowerCase()).join('-');
}

function esPalabraPar(palabra) {
  return palabra.length % 2 === 0;
}

function empiezaPorMays(palabra) {
  return palabra.charAt(0) === palabra.charAt(0).toUpperCase();
}

function analizaPalabras() {
  let palabras = this;
  for (let i in palabras) {
    let palabra = palabras[i];
    console.log(
      `    ############
    Palabra ${+i + 1}: ${palabra}
    Número de caracteres: ${palabra.length}
    Deletreo: ${palabra.deletrea()}
    La palabra es ${esPalabraPar(palabra) ? 'par' : 'impar'} y ${empiezaPorMays(palabra) ? '' : 'no '}empieza por mayúscula`,
    );
  }
  console.log('    ############');
}

let palabras = ['hola', 'Fotosíntesis', 'Fusible', 'yo'];

Array.prototype.analiza = analizaPalabras;
String.prototype.deletrea = deletrea;
palabras.analiza();
